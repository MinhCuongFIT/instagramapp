<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('../node_modules/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('../owlcarousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('../owlcarousel/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('./src/css/styles.css') }}">
    @yield('styles')
</head>
<body>
    @include('partials.header')
    @include('partials.carousel')
    <div class="container-fluid">
        @yield('content')
    </div>
    <script  type="text/javascript" src="{{ asset('../node_modules/jquery/dist/jquery.min.js') }}"></script>
    <script  type="text/javascript" src="{{ asset('../node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script  type="text/javascript" src="{{ asset('../jquery.min.js') }}"></script>
    <script  type="text/javascript" src="{{ asset('../owlcarousel/owl.carousel.min.js') }}"></script>
    @yield('scripts')
</body>
</html>
